<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MattyRad\NounProject;
use Intervention\Image\ImageManagerStatic as Image;

class PictoPlotsController extends Controller
{
    public function index()
     {
  //    	$api = new NounProject\Client($key = '1663816a891a429ea0aa2a80b6de6a5c', $secret= '8085191c0a6c44cda9a31f56816baddc');
		// $result = $api->send(new NounProject\Request\Icons($term = 'feather', $public_domain = true));

		// if (! $result->isSuccess()) {
		//     dd($result);
		// }

		// $icons = $result->getIcons();
     	$flag = 0;
     	while($flag == 0)
     	{
     	$randomKey = $this->randomCharactor();
     	$url = 'http://www.omdbapi.com/?t='.$randomKey.'&apikey=14c8c5bf&plot=full';
     	$json = file_get_contents($url);
		$json_data = json_decode($json, true); 
		//make sure Response == true and poster exist
		if($json_data['Response'] == "True" && $this->checkRemoteFile($json_data['Poster']))
		{
			$flag = 1;
		}
     	}
     	$img = Image::make($json_data['Poster']);
		// apply stronger blur
		$img->blur(15);

         return view('index',$json_data); 
     }

    function randomCharactor(){
 $length = 2;
return $randomletter = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, $length);
}

function checkRemoteFile($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    // don't download content
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    curl_close($ch);
    if($result !== FALSE)
    {
        return true;
    }
    else
    {
        return false;
    }
}

 }
