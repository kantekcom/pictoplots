<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Picto Plots</title>
      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <!-- Styles -->
      <style>
         html, body {
         background-color: #fff;
         color: #636b6f;
         font-family: 'Nunito', sans-serif;
         font-weight: 200;
         height: 100vh;
         margin: 0;
         }
         .full-height {
         height: 100vh;
         }
         .flex-center {
         align-items: center;
         display: flex;
         justify-content: center;
         }
         .position-ref {
         position: relative;
         }
         .top-right {
         position: absolute;
         right: 10px;
         top: 18px;
         }
         .content {
         text-align: center;
         }
         .title {
         font-size: 84px;
         }
         .links > a {
         color: #636b6f;
         padding: 0 25px;
         font-size: 13px;
         font-weight: 600;
         letter-spacing: .1rem;
         text-decoration: none;
         text-transform: uppercase;
         }
         .m-b-md {
         margin-bottom: 30px;
         }
      </style>
   </head>
   <body>
      <div class="container position-ref full-height">
           <div class="row">
         <div class=" flex-center align-self-center">
            <h3 class = 'text-primary'>Please click in two suggestions bellow and think in your mine name of this film</h3>
         </div>
      </div>
      <div class ="row">
         <div class="content">
                <button type="button" id="btnShowPlot" class="btn btn-primary">Show Plot</button>
                     <div id="plot">
                     </div>
         </div>
      </div>      
      <div class ="row">
         <div class="content">
                <button type="button" id="btnShowPoster" class="btn btn-primary">Show Poster</button>
                     <div id="poster">
                     </div>
         </div>
      </div>
<hr class="mt-5 mb-5">
      <div class="row" style='padding:10px'>
                  <div class=" flex-center align-self-center">
            <h3 class = 'text-primary'>So, do you have the name of this film in your mine? Please click here to know if you have a right choose.</h3>
         </div>
            <div class ="row">
                  <div class="content">
                     <button type="button" id="btnShowAnswer" class="btn btn-primary">Show Correct Title</button>
                     <button onClick="window.location.reload();">Refresh Page</button>
                     <div id="answer" style="color:red">
                     </div>
                  </div>
            </div>
         </div>
      </div>
   </body>
   <script type="text/javascript">
      var title = html_encode("<?php echo $Title; ?>");
      var plot = html_encode("<?php echo $Plot; ?>");
      var poster = html_encode("<img src=' <?php echo $Poster; ?> '/>");
          $("#btnShowAnswer").one('click',function () {
          $("#answer").append(title);
      });
          $("#btnShowPlot").one('click',function () {
          $("#plot").append(plot);
      });
          $("#btnShowPoster").one('click',function () {
          $("#poster").append(poster);
      });

      /**
 * Encodes special html characters
 * @param string
 * @return {*}
 */
function html_encode(string) {
    var ret_val = '';
    for (var i = 0; i < string.length; i++) { 
        if (string.codePointAt(i) > 127) {
            ret_val += '&#' + string.codePointAt(i) + ';';
        } else {
            ret_val += string.charAt(i);
        }
    }
    return ret_val;
}
   </script>
</html>